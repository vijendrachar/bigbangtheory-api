console.log('Hello Form the index.js File');

const getData = async () => {
  const CORSanywhere = `https://cors-anywhere.herokuapp.com/`;
  const res = await axios.get(
    `${CORSanywhere}https://s3-ap-southeast-1.amazonaws.com/edwisor-india-bucket/assignments/web03/JSS1l2/bigbangtheory.json`
  );
  return res;
};

const getDetialsBySeasonAndEpisodeNo = async (seas, eps) => {
  const res = await getData();
  let episodeDetails = [];

  res.data._embedded.episodes.map((episode) => {
    if (episode.season == seas && episode.number == eps) {
      episodeDetails.push(episode);
    }
  });

  if (episodeDetails.length > 0) {
    console.log(episodeDetails[0]);
    return episodeDetails[0];
  } else {
    console.log(`No Details about Season ${seas} with Episode ${eps}`);
    return `No Details about Season ${seas} with Episode ${eps}`;
  }
};

const getDetailsByMultipleEpisodeIDs = async (arr) => {
  const res = await getData();

  const resData = res.data._embedded.episodes;

  const searchArr = [];

  console.log('Function is live');
  resData.forEach((element, index) => {
    arr.forEach((ele) => {
      if (ele == resData[index].id) {
        searchArr.push(element);
      }
    });
  });

  if (searchArr.length > 0) {
    console.log(searchArr);
    return searchArr;
  } else {
    console.log(`There are no Episodes with those IDs`);
    return `There are no Episodes with those IDs`;
  }
};

const getDetailsByEpisodeName = async (epsName) => {
  const res = await getData();
  epsName = epsName.toLowerCase();
  const searchRes = [];

  res.data._embedded.episodes.forEach((episode) => {
    if (episode.name.toLowerCase().includes(epsName)) {
      searchRes.push(episode);
    }
  });

  if (searchRes.length > 0) {
    console.log(searchRes);
    return searchRes;
  } else {
    console.log(`There are no episodes with the name '${epsName}'`);
    return `There are no episodes with the name '${epsName}'`;
  }
};

// The DOM Section

const search = document.getElementById('search');
const searchOption = document.getElementById('searchOpt');
const searchForm = document.getElementById('searchForm');
const cardContainer = document.getElementById('cardContainer');

const renderCards = (episodeData) => {
  const card = `
  <div class="card mb-3">
        <img class="card-img-top" src="${episodeData?.image?.original}" alt="Card image cap" />
        <div class="card-body">
          <h2 class="card-title">${episodeData?.name}</h5>
          <p class="card-text">
            Episode ID : ${episodeData?.id}
          </p>
          <p class="card-text">
            ${episodeData?.summary}
          </p>
          <p class="card-text">
            Season : ${episodeData?.season} & Episode : ${episodeData?.number}
          </p> 
          <p class="card-text">
            Watch Time: ${episodeData?.airtime} mins
          </p>
          <a href="${episodeData?.url}" target="_blank" class="btn btn-primary active" role="button">Learn More</a>
        </div>
    </div>
  `;

  cardContainer.insertAdjacentHTML('afterbegin', card);
};

const renderErrMessage = (message) => {
  const mess = `<p>${message}</p>`;

  cardContainer.insertAdjacentHTML('afterbegin', mess);
};

const clearRes = () => {
  cardContainer.innerHTML = '';
};

searchForm.addEventListener('submit', async (e) => {
  e.preventDefault();
  clearRes();

  if (searchOption.value === 'seasAndEps') {
    const seasAndEps = search.value.split(',');
    const epiData = await getDetialsBySeasonAndEpisodeNo(
      seasAndEps[0],
      seasAndEps[1]
    );

    if (epiData.name) {
      renderCards(epiData);
    } else {
      renderErrMessage(epiData);
    }
  } else if (searchOption.value === 'epsId') {
    const arrIDs = search.value.split(',');
    const searchEpiData = await getDetailsByMultipleEpisodeIDs(arrIDs);

    if (Array.isArray(searchEpiData)) {
      searchEpiData.forEach((epi) => {
        renderCards(epi);
      });
    } else {
      renderErrMessage(searchEpiData);
    }
  } else if (searchOption.value === 'epiName') {
    const searchName = search.value;
    const searchNameRes = await getDetailsByEpisodeName(searchName);

    if (Array.isArray(searchNameRes)) {
      searchNameRes.forEach((epi) => {
        renderCards(epi);
      });
    } else {
      renderErrMessage(searchNameRes);
    }
  }
});
